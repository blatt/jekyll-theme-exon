# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-theme-exon'
  spec.version       = '0.1.0'
  spec.authors       = ['Daniel Blatt']
  spec.email         = ['lynx@mailbox.org']

  spec.summary       = 'My custom Jekyll theme. Used on my personal website.'
  spec.homepage      = 'https://gitlab.com/blatt/jekyll-theme-exon'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency 'html-proofer'
  spec.add_runtime_dependency 'jekyll', '~> 3.7'
  spec.add_runtime_dependency 'jekyll-feed'
  spec.add_runtime_dependency 'jekyll-paginate'
  spec.add_runtime_dependency 'jekyll-sitemap'
  # spec.add_runtime_dependency 'rdiscount'

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 12.0'
end
